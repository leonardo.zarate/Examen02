﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int score;
    public Text textScore;
    public int life = 3;
    public Text Lifes;
    public Vector2 FirstPosition, SeconPosition;
    public Vector2 CurrentSwipe;
    public Text swipes;
    // Start is called before the first frame update
    void Start()
    {
        textScore.text = "0";
        Lifes.text = this.life.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayerControl()
    {
        if(Input.touchCount> 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    FirstPosition = new Vector2(touch.position.x, touch.position.y);

                    break;
                case TouchPhase.Ended:
                    SeconPosition = new Vector2(touch.position.x, touch.position.y);
                    CurrentSwipe = new Vector2(SeconPosition.x - FirstPosition.x, SeconPosition.y - FirstPosition.y);                 
                    if(CurrentSwipe.y >0 && CurrentSwipe.x > -0.5f && CurrentSwipe.x <0.5f)
                    {
                        swipes.text = "UP SWIPE";

                    }else if(CurrentSwipe.y < 0 && CurrentSwipe.x > -0.5f && CurrentSwipe.x < 0.5f)
                    {
                        swipes.text = "DOWN SWIPE";
                    }
                    break;
                default:
                    break;
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
            this.life--;
            Lifes.text = "Lives: " + this.life.ToString();
        }
        if(collision.gameObject.tag == "points")
        {
            Destroy(collision.gameObject);
            HitPoint();
        }
    }
    public void HitPoint()
    {
        this.score += 10;
        this.textScore.text = "Score: " + this.score.ToString();
    }
}
