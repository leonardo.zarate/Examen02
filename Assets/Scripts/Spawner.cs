﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float spawnrate=0.2f;
    public float pointsrate;
    public GameObject[] Spawners;
    public GameObject enemy;
    public GameObject points;
    private int spawnerid;
    bool waveIsDone = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (waveIsDone == true)
            StartCoroutine(Summon());
    }
    IEnumerator Summon()
    {
        waveIsDone = false;
        spawnerid = Random.Range(0, 2);
        spawnrate = Random.Range(0.25f, 1.0f);
        pointsrate = Random.Range(0,15);

        float izq = Random.Range(Spawners[0].transform.position.y, 2.7f);
        float der = Random.Range(Spawners[1].transform.position.y, 2.7f);

        if(spawnerid == 0)
        {
            if(pointsrate >7)
            {
                points.transform.position = new Vector2(Spawners[0].transform.position.x, izq);
                GameObject pointsclone = Instantiate(points);
            }
            else
            {
                enemy.transform.position = new Vector2(Spawners[0].transform.position.x, izq);
                GameObject enemyclone = Instantiate(enemy);
            }
            yield return new WaitForSeconds(spawnrate);
        }
        else if(spawnerid == 1)
        {
            if (pointsrate > 7)
            {
                points.transform.position = new Vector2(Spawners[1].transform.position.x, der);
                GameObject pointsclone = Instantiate(points);
            }
            else
            {
                enemy.transform.position = new Vector2(Spawners[1].transform.position.x, der);
                GameObject enemyclone = Instantiate(enemy);
            }
            yield return new WaitForSeconds(spawnrate);
        }


        yield return new WaitForSeconds(spawnrate);
        waveIsDone = true;
    }
}
