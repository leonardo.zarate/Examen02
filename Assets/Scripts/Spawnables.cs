﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawnables : MonoBehaviour
{
    public float speed = 10.0f;
    private Rigidbody2D rb;
    private Vector2 screenBounds;


    // Use this for initialization
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        Movement();
        

    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x < -2.5 || this.transform.position.x > 2.5)
        {
            Destroy(this.gameObject);
        }
    }

    private void Movement()
    {
        if(this.transform.position.x == -2)
        {
            rb.velocity = new Vector2(speed, 0);
        }
        else if(this.transform.position.x == 2)
        {
            rb.velocity = new Vector2(-speed, 0);
        }
    }
}
